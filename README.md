## AUTH API EXAMPLE

- to get token:
```
  GET /api/v1/users/token
```

- create new account

```
  POST /api/v1/users
  body: user, email, Password, verify_password
  eg: {"username": "diepmv01", "password": "test", "verify_password": "test", "email": "maidiep26101@gmail.com"}
```

- Auth header example

```
    {"Authorization": "Token eyJhbGciOiJIUzUxMiIsImlhdCI6MTU3MzA4OTkxNSwiZXhwIjoxNTczMDkzNTE1fQ.eyJpZCI6MX0.QQYlpARfLLMEthspHYdzCfF519PVFW-Q5b1njgacLS4rOzyYODDajyxYmQeH9MyB7qoD1XLYQK-XV_HMiO4C2g"} 
```